package com.example.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

public class MyTextView extends View {
	
	Paint p; // Paint object is used to control the drawing style

	public MyTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// To initialize your view
		
		p = new Paint();
		p.setAntiAlias(true);
		p.setColor(Color.BLUE);
		p.setTextSize(dpToPixels(30)); // move from method to here for calling only once
		
	}

	// This method will be used to draw (display some contents) our view
	// when android tries to show it to user
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		// canvas object is used to draw the graphics.
		// it can be used inside this onDraw method only.
		
		// draw a circle with radius 50 at (100,100)
		canvas.drawCircle(100, 100, 50, p);
		
		//draw a line from (0,0) to (50,50)
		canvas.drawLine(0,0,50,50,p);
		
		
		String s = "Hello, Android!!!";
		
		// get dimension of view and text
		p.getTextBounds(s, 0, s.length(), rect);
		float textWidth = rect.width();
		float textHeight = rect.height();
		
		float viewHeight = this.getHeight();
		float viewWidth = this.getWidth();
		
		if(x == 0)
		{
			canvas.drawText(s, 0 , (viewHeight+textHeight)/2 , p);
		}
		else if(x == 1)
		{
			canvas.drawText(s, (viewWidth-textWidth)/2 , (viewHeight+textHeight)/2 , p);
		}
		else if(x == 2)
		{
			canvas.drawText(s, viewWidth-textWidth , (viewHeight+textHeight)/2 , p);
		}
		
		
	}
	
	Rect rect = new Rect(); // An object to store the dimension of text
	
	protected float dpToPixels(float dp){
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
	}	
	
	public void setTextSize(float s){
		p.setTextSize(dpToPixels(s));
		invalidate(); // To force the MyTextView to be redrawn
	}
	
	int x;
	public void setAlign(int v){
		this.x = v;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int a = event.getAction();
		if(a == MotionEvent.ACTION_DOWN){
			this.setBackgroundColor(Color.YELLOW);
			invalidate();
			return true; // to specify that the event has been handled.
		}
		else if(a == MotionEvent.ACTION_MOVE){
			this.setBackgroundColor(Color.RED);
			invalidate();
			return true;
		}
		else if(a == MotionEvent.ACTION_UP){
			this.setBackgroundColor(Color.GREEN);
			invalidate();
			return true;
		}
		return super.onTouchEvent(event);
	}
	
	
}
