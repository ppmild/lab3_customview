import android.view.View;
import android.graphics.*;

public class Circle extends View {
	float x, y;
	int color;
	
	public Circle(float x, float y, int c) {
		super(null, null, c);
		this.x = x;
		this.y = y;
		this.color = c;	
	}
}
